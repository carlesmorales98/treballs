<?php

require_once(__DIR__.'/../controller/IndexController.php');

$cnt = new IndexController();
$list = $cnt->Frases();

?><html>
    <head>
        <title>Frase Filosoficas</title>
    </head>
    <body>
        <div id="wrapper">
            <h1>Frases Filosoficas</h1>
            <table>
                <tr><th>Autor</th><th>Categoria</th><th>Frase</th></tr>
                <?php foreach($list as $frase){ ?>
                <tr>
                    <td><?=$frase->getNom()?></td>
                    <td><?=$frase->getCategoria()?></td>
                    <td><?=$frase->getFrase()?></td>
                </tr>
                <?php } ?>
            </table>
        </div>
    </body>
</html>
