<?php

class TuitDb{

    public function getTuitById($tuid){
        $collection = (new MongoDB\Client)->tuita->tuits;
        $reg = $collection->findOne(["_id"=>new MongoDB\BSON\ObjectId($tuid)]);
        $tuit = new Tuit($reg['msg'],
                $reg['usr'],
                $reg['fav'],
                $reg['_id']->__toString());

        return $tuit;
    }

    public function createTuit($message, $username){
        $nt = new Tuit($message, $username, 0);

        $collection = (new MongoDB\Client)->tuita->tuits;

        $insertOneResult = $collection->insertOne($nt->toArray());
        $nt->setTuid($insertOneResult->getInsertedId()->__toString());

        return $nt;
    }

    public function favTuit($tuid){
        $collection = (new MongoDB\Client)->tuita->tuits;
        $collection->updateOne(["_id"=>new MongoDB\BSON\ObjectId($tuid)],
            ['$inc' => ['fav'=>1]]);

        return $this->getTuitById($tuid);
    }

}
