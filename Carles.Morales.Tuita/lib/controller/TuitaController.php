<?php

require_once(__DIR__.'/../model/Tuit.php');
require_once(__DIR__.'/../model/db/TuitDb.php');

class TuitaController{

    public function postTuit($msg, $un){
        $db = new TuitDb();
        $tuit = $db->createTuit($msg, $un);

        return $tuit;
    }

    public function fav($tuid){
        $db = new TuitDb();
        $tuit = $db->favTuit($tuid);

        return $tuit;
    }

}
