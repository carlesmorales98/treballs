<?php

require_once(__DIR__.'/../controller/IndexController.php');

$cnt = new IndexController();
$carlist = $cnt->randomListAction();

?><html>
    <head>
        <title>MVC Car Sample</title>
    </head>
    <body>
        <div id="wrapper">
            <h1>MVC Car Sample</h1>
            <table>
                <tr><th>Marca</th><th>Model</th><th>Combustible</th><th>Color</th></tr>
                <?php foreach($carlist as $car){ ?>
                <tr>
                    <td><?=$car->getBrand()?></td>
                    <td><?=$car->getModel()?></td>
                    <td><?=$car->getGas()?></td>
                    <td><?=$car->getColor()?></td>
                </tr>
                <?php } ?>
            </table>
        </div>
    </body>
</html>
