<?php

require_once(__DIR__.'/../controller/GameCController.php');
$cnt = new GameCController();

$list = $cnt->listCharacters();

?><html>
	<head>
	</head>
	<body>
		<div id="wrapper">
			<div id="homeform">
				<form method="post" action="/forms/index.php">
					<dl>
						<!-- Name -->
						<dt><label for="cname">Name</label></dt>
						<dd><input type="text" id="cname" name="cname"/></dd>
						<!-- Specie -->
						<dt><label for="cspec">Specie</label></dt>
						<dd>
							<select id="cspec" name="cspec">
								<option value="Turtle">Turtle</option>
								<option value="Snake">Snake</option>
								<option value="Rabbit">Rabbit</option>
								<option value="Rat">Rat</option>
							</select>
						</dd>
						<!-- Attack -->
						<dt><label for="cap">Attack Points</label></dt>
						<dd><input type="numeric" id="cap" name="cap"/></dd>
						<!-- Defense -->
						<dt><label for="cdp">Defense Points</label></dt>
						<dd><input type="numeric" id="cdp" name="cdp"/></dd>
						<dd><input type="submit" name="csubmit" value="Add"/></dd>
					</dl>
				</form>
			</div>
			<hr/>
    		<h2>Character List</h2>
    		<ul>
    		<?php foreach($list as $g){ ?>
    		<li><?=$g->getName()?>:<?=$g->getSpecie()?></li>
    		<?php } ?>
    		</ul>
		</div>
	</body>
</html>
